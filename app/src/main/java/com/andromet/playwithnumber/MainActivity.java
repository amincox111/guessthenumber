package com.andromet.playwithnumber;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    EditText editTextNumber;
    TextView textViewCheck;
    String strNumber;
    int number, randomNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextNumber = findViewById(R.id.editTextNumber);
        textViewCheck = findViewById(R.id.textViewCheck);

        Random random = new Random();
        randomNumber = random.nextInt(20) + 1;

    }

    public void onCheckClicked(View view) {

        strNumber = editTextNumber.getText().toString();
        number = Integer.parseInt(strNumber);

        if(number == randomNumber)
        {
            textViewCheck.setText("Yes!! You Win!!");
        }
        else if (number > randomNumber)
        {
            textViewCheck.setText("Oh!! It's higher!!");
        }
        else
        {
            textViewCheck.setText("Oh!! It's lower!!");
        }
    }
}
